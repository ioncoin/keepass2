# KeePass Password Management

- [KeePass Password Management](#keepass-password-management)
  - [Installation (install keepass)](#installation-install-keepass)
    - [Ubuntu/Debian](#ubuntudebian)
      - [Install](#install)
  - [Plugins](#plugins)
    - [Tray TOTP Plugin for KeePass 2](#tray-totp-plugin-for-keepass-2)
      - [Install TrayTOTP plugin](#install-traytotp-plugin)
    - [YetAnotherFaviconDownloader](#yetanotherfavicondownloader)
      - [Install YetAnotherFaviconDownloader plugin](#install-yetanotherfavicondownloader-plugin)
    - [SftpSync plugin](#sftpsync-plugin)
      - [Install SftpSync plugin](#install-sftpsync-plugin)
    - [ReadablePassphrase plugin](#readablepassphrase-plugin)
      - [Install ReadablePassphrase plugin](#install-readablepassphrase-plugin)

**WARNING**: _This KeePass 2 plugin is compatible with version 2.21+ of KeePass as earlier versions do not include features that are used by the plugin_.

## Installation (install keepass)
### Ubuntu/Debian

- Update and install keepass2 and required dependencies for keepass2
  ```sh
  export PLUGINDIR="/usr/lib/keepass2/Plugins"

  sudo apt-get update; 
  sudo apt-get install -y mono-complete keepass2 xdotool ttf-mscorefonts-installer;

  # Install all plugins
  sudo mkdir -p "${PLUGINDIR}"
  cd "${PLUGINDIR}"
  sudo git clone https://github.com/cevap/keepass2.git
  ```

#### Install
```

cd "${PLUGINDIR}"
sudo wget https://github.com/cevap/keepass2/raw/master/TrayTotp/TrayTotp.plgx
```

## Plugins

[Find more plugins](https://keepass.info/plugins.html).

### Tray TOTP Plugin for KeePass 2

Version 2.0.0.5, [TrayTotp plugin Readme](TrayTotp/Tray%20TOTP%20v.%202.0.0.5%20Readme.txt)

#### Install TrayTOTP plugin
```
export PLUGINDIR="/usr/lib/keepass2/Plugins/TrayTotp"
sudo mkdir -p "${PLUGINDIR}"
cd "${PLUGINDIR}"
sudo wget https://github.com/cevap/keepass2/raw/master/TrayTotp/TrayTotp.plgx
```

_Download [latest original source version](https://sourceforge.net/projects/traytotp-kp2/)_.

_Author's official web site: http://traytotp-kp2.sourceforge.net/_

### YetAnotherFaviconDownloader
#### Install YetAnotherFaviconDownloader plugin
```
export PLUGINDIR="/usr/lib/keepass2/Plugins/YetAnotherFaviconDownloader"
sudo mkdir -p "${PLUGINDIR}"
cd "${PLUGINDIR}"
sudo wget https://github.com/cevap/keepass2/raw/master/YetAnotherFaviconDownloader/YetAnotherFaviconDownloader.plgx
```
_Author's official web site: https://github.com/navossoc/KeePass-Yet-Another-Favicon-Downloader/_

### SftpSync plugin
#### Install SftpSync plugin
```
export PLUGINDIR="/usr/lib/keepass2/Plugins/SftpSync"
sudo mkdir -p "${PLUGINDIR}"
cd "${PLUGINDIR}"
sudo wget https://github.com/cevap/keepass2/raw/master/SftpSync/SftpSync.plgx
```

_Download [latest original source version](https://sourceforge.net/projects/keepass-sftp-sync/files/)_.

_Author's official web site: https://sourceforge.net/projects/keepass-sftp-sync/_

### ReadablePassphrase plugin
#### Install ReadablePassphrase plugin
```
export PLUGINDIR="/usr/lib/keepass2/Plugins/ReadablePassphrase"
sudo mkdir -p "${PLUGINDIR}"
cd "${PLUGINDIR}"
sudo wget https://github.com/cevap/keepass2/raw/master/ReadablePassphrase/ReadablePassphrase.plgx
```

_Download [latest original source version](https://bitbucket.org/ligos/readablepassphrasegenerator/downloads/)_.

_Author's official web site: https://bitbucket.org/ligos/readablepassphrasegenerator/overview_